(function() {
//
// 定数群、コンテスト問題ごとに変更して使用する
//
var USE_IMAGE = false;
var ONTIME_SUBMITS = 100;
var AFTER_SUBMITS = 110;
var SUBMITS = ONTIME_SUBMITS;
var ABS = 'ABS', REL = 'REL', BEAT = 'BEAT';
var SCORE_TYPE = [ABS, REL, BEAT][1];
var SCORE_ASC_ORDER = false;
var FIRST_SEED_IN_TXT  = 1;
var LAST_SEED_IN_TXT   = 100;
var FIRST_SEED_OF_VIEW = FIRST_SEED_IN_TXT;
var LAST_SEED_OF_VIEW  = LAST_SEED_IN_TXT;
var DEFAULT_VIEW_SEED  = FIRST_SEED_OF_VIEW;
var SCORE_LEN = 0;
var SHOW_RATE = false;
var RATE_SCALE = 100.0;
var SCORE_PARSER = /Score\s+=\s+(\S+)/;
// ./data.txt
var USE_TSCORE = false;
var COMP_TSCORE = true;
var TSCORE_PARSER = /MaxScore:\s+(\S+)/;
var USE_INFO = true;
var INFO_NAMES = ['S', 'N', 'W', 'H', 'H/S2', 'P', 'los', 'area', 'r,c'];
var INFO_PARSERS = [
	/S:\s+(\S+)/,
	/N:\s+(\S+)/,
	/W:\s+(\S+)/,
	/H:\s+(\S+)/,
	/H.S2:\s+(\S{5})/,
	/P:\s+(\S{5})/,
	/los:\s+(\S+)/,
	/area:\s+(\S+)/,
	/r,c:\s+(\S+)/
	];
//
// plog ページ内にデバッグメッセージを表示する
//
function plog(msg, ins) {
	var d = document.getElementById('debuglog');
	if (d == undefined) {
		d = document.createElement('div');
		d.className = 'debuglog';
		d.id = 'debuglog';
		document.body.appendChild(d);
	}
	if (!ins) {
		d.innerHTML = '';
	}
	d.appendChild(document.createTextNode(msg));
}
//
// dlog ブラウザコンソールにデバッグメッセージを表示する
//
function dlog(msg) {
	if (console) { if (console.log) {
		console.log(msg);
		return true;
	}}
	return false;
}
//
// log ブラウザコンソールまたはページ内にデバッグメッセージを表示する
//
function log(msg) {
	if (!dlog(msg)) { plog(msg, true); }
}
//
// XMLHttpRequestオブジェクト生成
//
var xhr = (function() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	}
	if (window.ActiveXObject) {
		try {
			return new ActiveXObject('Microsoft.XMLHTTP');
		} catch (_) {}
		try {
			return new ActiveXObject('Msxml2.XMLHTTP');
		} catch (_) {}
	}
	return {
		'open': function() {},
		'send': function() {}
	};
})();
//
// OrdName 数字を序数名に変換する
//
function OrdName(n) {
	var mod10 = n % 10;
	var mod100 = n % 100;
	if (mod10 === 1 && mod100 !== 11) {
		return n.toString() + 'st';
	}
	if (mod10 === 2 && mod100 !== 12) {
		return n.toString() + 'nd';
	}
	if (mod10 === 3 && mod100 !== 13) {
		return n.toString() + 'rd';
	}
	return n.toString() + 'th';
}
//
// formatFloat 小数以下の桁数dで小数vを文字列表現する
//
function formatFloat(v, d) {
	if (typeof v !== 'number') { return 'ERROR'; }
	if (typeof d !== 'number' || d < 2) { d = 2; }
	var ds = '';
	for (var i = 0; i < d; i++) { ds += '0'; }
	var s = v.toString().split('.');
	if (s.length < 2) {
		return s[0] + '.' + ds;
	}
	return s[0] + '.' + (s[1] + ds).substring(0, d);
}
//
// 各種スコア保存用変数
//
// ※ 0-indexにするためSUBMIT値やSEED値を-1して使用している
//
var scores  = new Array();  // 各SUBMITの各SEEDのスコア
var points  = new Array();  // 各SUBMITの総合スコア
var bests   = new Array();  // 各SEEDの一番良いスコア
var seconds = new Array(); // 各SEEDの二番目に良いスコア
var thirds  = new Array();  // 各SEEDの三番目に良いスコア
var worsts  = new Array();  // 各SEEDの一番悪いスコア
var boobys  = new Array();  // 各SEEDの二番目に悪いスコア
var uniques = new Array();  // 各SEEDのBESTの数
var bestcounts   = new Array(); // SUBMITごとのBESTの数
var secondcounts = new Array();
var thirdcounts  = new Array();
var boobycounts  = new Array();
var worstcounts  = new Array();
var uniquecounts = new Array();
var errorcounts = new Array();
var tscores = new Array();
var infos = new Array();
//
// findSpecialScores 色づけのための各SEEDのベストスコアやワーストスコアを求める
//
function findSpecialScores() {
	var numOfSc = LAST_SEED_IN_TXT - FIRST_SEED_IN_TXT + 1;
	for (var i = 0; i < numOfSc; i++) {
		var seed = i + FIRST_SEED_IN_TXT;
		if (seed < FIRST_SEED_OF_VIEW || LAST_SEED_OF_VIEW < seed) { continue; }
		var ar = new Array();
		var es = new Array();
		for (var j = 0; j < SUBMITS; j++) {
			if (SCORE_ASC_ORDER && scores[j][i] < 0.0) {
				ar[j] = Number.POSITIVE_INFINITY;
			} else {
				ar[j] = scores[j][i];
			}
			es[j] = scores[j][i];
		}
		ar.sort(function(a, b) { return SCORE_ASC_ORDER ? a - b : b - a; });
		if (SCORE_ASC_ORDER) {
			for (var j = 0; j < SUBMITS; j++) {
				if (ar[j] === Number.POSITIVE_INFINITY) {
					ar[j] = es[j];
				}
			}
		}
		bests[i] = ar[0];
		if (USE_TSCORE === false) {
			tscores[i] = bests[i];
		}
		uniques[i] = 1;
		for (var j = 1; j < SUBMITS; j++) {
			if (ar[j] === bests[i]) {
				uniques[i]++;
			} else {
				break;
			}
		}
		for (var j = SUBMITS - 1; j >= 0; j--) {
			if (ar[j] < 0.0) { continue; }
			worsts[i] = ar[j];
			break;
		}
		for (var j = 1; j < SUBMITS; j++) {
			if (ar[j] === bests[i]) { continue; }
			seconds[i] = ar[j];
			break;
		}
		for (var j = 2; j < SUBMITS; j++) {
			if (ar[j] === bests[i]) { continue; }
			if (ar[j] === seconds[i]) { continue; }
			thirds[i] = ar[j];
			break;
		}
		for (var j = SUBMITS - 2; j >= 0; j--) {
			if (ar[j] === worsts[i]) { continue; }
			boobys[i] = ar[j];
			break;
		}
	}
	for (var j = 0; j < SUBMITS; j++) {
		var bs = 0, sn = 0, bb = 0, ws = 0, th = 0, un = 0, er = 0;
		for (var i = 0; i < numOfSc; i++) {
			if (scores[j][i] === bests[i]) {
				bs++;
				if (uniques[i] === 1) {
					un++;
				}
			} else if (scores[j][i] === seconds[i]) {
				sn++;
			} else if (scores[j][i] === thirds[i]) {
				th++;
			} else if (scores[j][i] === boobys[i]) {
				bb++;
			} else if (scores[j][i] === worsts[i]) {
				ws++;
			} else if (scores[j][i] < 0.0) {
				er++;
			}
		}
		bestcounts[j]   = bs;
		secondcounts[j] = sn;
		thirdcounts[j]  = th;
		boobycounts[j]  = bb;
		worstcounts[j]  = ws;
		uniquecounts[j] = un;
		errorcounts[j]  = er;
	}
}
//
// showRanking スコア表を表示する(tpでソート対象を指定する)
//
// tp
//  負数 ... 順位でソート
//  0    ... 総合スコアでソート
//  正数 ... tpをSEEDとしてSEEDケースのスコアでソート
function showRanking(tp) {
	var rk = new Array();
	if (tp < 0) {
		for (var j = 1; j <= SUBMITS; j++) {
			rk[j-1] = [SUBMITS - j + 1];
		}
	} else if (tp === 0) {
		for (var j = 1; j <= SUBMITS; j++) {
			rk[j-1] = [j, points[j-1]];
		}
		rk.sort(function(a, b) {
			return (SCORE_ASC_ORDER && SCORE_TYPE == ABS) ? a[1] - b[1] : b[1] - a[1];
		});
	} else {
		for (var j = 1; j <= SUBMITS; j++) {
			rk[j-1] = [j, scores[j-1][tp-1]];
		}
		rk.sort(function(a, b) {
			return SCORE_ASC_ORDER ? a[1] - b[1] : b[1] - a[1];
		});
	}
	for (var r = 1; r <= SUBMITS; r++) {
		var j = rk[r-1][0];
		document.getElementById('rbsb' + r.toString())
			.innerHTML = OrdName(j);
		document.getElementById('rbsc' + r.toString())
			.innerHTML = formatFloat(points[j-1]);
		document.getElementById('rbun' + r.toString())
			.innerHTML = uniquecounts[j-1].toString();
		document.getElementById('rbbs' + r.toString())
			.innerHTML = bestcounts[j-1].toString();
		document.getElementById('rbsn' + r.toString())
			.innerHTML = secondcounts[j-1].toString();
		document.getElementById('rbti' + r.toString())
			.innerHTML = thirdcounts[j-1].toString();
		document.getElementById('rbbb' + r.toString())
			.innerHTML = boobycounts[j-1].toString();
		document.getElementById('rbws' + r.toString())
			.innerHTML = worstcounts[j-1].toString();
		document.getElementById('rber' + r.toString())
			.innerHTML = errorcounts[j-1].toString();
		for (var i = FIRST_SEED_IN_TXT; i <= LAST_SEED_IN_TXT; i++) {
			if (i < FIRST_SEED_OF_VIEW || LAST_SEED_OF_VIEW < i) { continue; }
			var id = 'rbex' + r.toString() + 'no' + i.toString();
			var e = document.getElementById(id);
			var sc = scores[j-1][i-1];
			if (sc === bests[i-1]) {
				e.className = 'bestscore';
			} else if (sc === seconds[i-1]) {
				e.className = 'betterscore';
			} else if (sc === thirds[i-1]) {
				e.className = 'thirdscore';
			} else if (sc === boobys[i-1]) {
				e.className = 'wrongscore';
			} else if (sc === worsts[i-1] || sc < 0.0) {
				e.className = 'worstscore';
			} else {
				e.className = '';
			}
			var rate = sc;
			if (rate > 0.0) {
				var besc = bests[i - 1];
				if (USE_TSCORE && COMP_TSCORE) {
					besc = tscores[i - 1];
				}
				if (SCORE_ASC_ORDER) {
					rate = RATE_SCALE * besc / sc;
				} else {
					rate = RATE_SCALE * sc / besc;
				}
			}
			if (SHOW_RATE) {
				e.title = OrdName(j) + ' submit: seed=' + i.toString()
					+ ', score=' + formatFloat(sc, SCORE_LEN);
				e.innerHTML = formatFloat(rate, SCORE_LEN);
			} else {
				e.title = OrdName(j) + ' submit: seed=' + i.toString()
					+ ', rate=' + formatFloat(rate, SCORE_LEN);
				e.innerHTML = formatFloat(sc, SCORE_LEN);
			}
		}
	}
}
//
// showScores 画像表示部に添えるスコアを表示する(iはSEED)
//
function showScores(i) {
	for (var j = 1; j <= SUBMITS; j++) {
		var id = 'score' + j.toString();
		var e = document.getElementById(id);
		var sc = scores[j-1][i-1];
		if (sc === bests[i-1]) {
			e.className = 'bestscore';
		} else if (sc === seconds[i-1]) {
			e.className = 'betterscore';
		} else if (sc === thirds[i-1]) {
			e.className = 'thirdscore';
		} else if (sc === boobys[i-1]) {
			e.className = 'wrongscore';
		} else if (sc === worsts[i-1]) {
			e.className = 'worstscore';
		} else {
			e.className = '';
		}
		// e.innerHTML = formatFloat(sc, SCORE_LEN);
		e.innerHTML = sc.toString();
	}
}
//
// changeImages コンボリストでの指定SEEDの画像とスコアを表示する
//
function changeImages() {
	var sel = document.getElementById('selexample').value;
	for (var j = 1; j <= SUBMITS; j++) {
		var id = 'img' + j.toString();
		var e = document.getElementById(id);
		e.className = 'example' + sel.toString();
		e.src = './submit' + j.toString() + '/' + sel.toString() + '.png';
	}
	showScores(sel);
}
//
// openBigimg クリックされた画像を元のサイズで表示する
//
function openBigimg(src) {
	var img = document.getElementById('bigimg');
	if (img.className.indexOf('bigimgon') >= 0) { return; }
	img.src = src;
	img.parentNode.className = 'bigimgon';
	var matches = src.match(/submit(\d+)\W(\d+)\W/);
	if (matches === null) {
		return;
	}
	var n = parseInt(matches[1]);
	var s = parseInt(matches[2]);
	var sc = scores[n - 1][s - 1];
	var e = document.getElementById('bigimgcap');
	if (sc === bests[s-1]) {
		e.className = 'bestscore';
	} else if (sc === seconds[s-1]) {
		e.className = 'betterscore';
	} else if (sc === thirds[s-1]) {
		e.className = 'thirdscore';
	} else if (sc === boobys[s-1]) {
		e.className = 'wrongscore';
	} else if (sc === worsts[s-1]) {
		e.className = 'worstscore';
	} else {
		e.className = '';
	}
	e.className += ' bigimgcap';
	e.innerHTML = '&nbsp' + OrdName(n) + ' : ' + sc.toString() + '&nbsp';
	var pr = n - 1;
	if (pr < 1) { pr = SUBMITS; }
	document.getElementById('bigimgprev').onclick = document.getElementById('img' + pr.toString()).onclick;
	var nx = n + 1;
	if (nx > SUBMITS) { nx = 1; }
	document.getElementById('bigimgnext').onclick = document.getElementById('img' + nx.toString()).onclick;
}
//
// closeBigimg 元のサイズで表示されていた画像を非表示にする
//
function closeBigimg() {
	document.getElementById('bigimg').parentNode.className = 'bigimgoff';
}
//
// buildImgTable 画像を並べるためのテーブルをページに埋め込む
//
function buildImgTable() {
	var tb = document.createElement('table');
	tb.border = 1;
	for (var i = 0; i < Math.ceil(SUBMITS / 4); i++) {
		var tr1 = document.createElement('tr');
		var tr2 = document.createElement('tr');
		for (var j = 0; j < 4; j++) {
			var n = i * 4 + j + 1;
			var nstr = OrdName(n);
			var td1 = document.createElement('td');
			var td2 = document.createElement('td');
			var td3 = document.createElement('td');
			var sp1 = document.createElement('span');
			var sp2 = document.createElement('span');
			td1.className = 'resulttitle';
			if (n <= SUBMITS) {
				sp1.innerHTML = nstr;
			}
			td1.appendChild(sp1);
			tr1.appendChild(td1);
			td2.className = 'score';
			sp2.id = 'score' + n.toString();
			if (n <= SUBMITS) {
				sp2.innerHTML = '0.0';
			}
			td2.appendChild(sp2);
			tr1.appendChild(td2);
			td3.colSpan = 2;
			if (n <= SUBMITS) {
				var img = document.createElement('img');
				img.id = 'img' + n.toString();
				img.className = 'example' + DEFAULT_VIEW_SEED.toString() + ' hand';
				img.src = './submit' + n.toString() + '/' + DEFAULT_VIEW_SEED.toString() + '.png';
				img.onclick = (function(im) {
					return function() {openBigimg(im.src); };
				})(img);
				td3.appendChild(img);
			}
			tr2.appendChild(td3);
		}
		tb.appendChild(tr1);
		tb.appendChild(tr2);
	}
	document.getElementById('imgtable').appendChild(tb);
}
//
// buildSelector 画像表示するSEEDを選択するためのフォームをページに埋め込む
//
function buildSelector() {
	var sel = document.createElement('select');
	sel.id = 'selexample';
	for (var i = FIRST_SEED_OF_VIEW; i <= LAST_SEED_OF_VIEW; i++) {
		var op = document.createElement('option');
		op.value = i.toString();
		if (i === DEFAULT_VIEW_SEED) { op.selected = true; }
		op.appendChild(
			document.createTextNode('Example Seed=' + i.toString())
		);
		sel.appendChild(op);
	}
	var btn = document.createElement('button');
	btn.className = 'hand';
	btn.onclick = changeImages;
	btn.appendChild(
		document.createTextNode('submit')
	);
	var e = document.getElementById('selector');
	e.appendChild(sel);
	e.appendChild(btn);
}
//
// calcPoints 総合スコアを計算する
//
function calcPoints() {
	var numOfSc = LAST_SEED_IN_TXT - FIRST_SEED_IN_TXT + 1;
	var numOfVw = LAST_SEED_OF_VIEW - FIRST_SEED_OF_VIEW + 1;
	if (SCORE_TYPE === BEAT) {
		for (var j = 1; j <= SUBMITS; j++) {
			var pt = 0.0;
			for (var i = 0; i < numOfSc; i++) {
				var e = i + FIRST_SEED_IN_TXT;
				if (e < FIRST_SEED_OF_VIEW || LAST_SEED_OF_VIEW < e) { continue; }
				var sc = scores[j-1][i];
				for (var k = 1; k <= SUBMITS; k++) {
					if (j === k) { continue; }
					if (sc < scores[k-1][i] && SCORE_ASC_ORDER) { pt += 1.0; }
					if (sc > scores[k-1][i] && !SCORE_ASC_ORDER) { pt += 1.0; }
					if (sc === scores[k-1][i]) { pt += 0.5; }
				}
			}
			if (SUBMITS == 1) {
				pt = numOfVw;
			}
			points[j-1] = (pt / Math.max(1.0, SUBMITS - 1.0)) * 1000000.0 / numOfVw;
		}
	} else if (SCORE_TYPE === REL) {
		for (var j = 1; j <= SUBMITS; j++) {
			points[j-1] = 0;
		}
		for (var i = 0; i < numOfSc; i++) {
			var e = i + FIRST_SEED_IN_TXT;
			if (e < FIRST_SEED_OF_VIEW || LAST_SEED_OF_VIEW < e) { continue; }
			var sc = scores[0][i];
			for (var j = 1; j <= SUBMITS; j++) {
				if (!SCORE_ASC_ORDER && sc < scores[j-1][i]) { sc = scores[j-1][i]; }
				if (SCORE_ASC_ORDER && sc > scores[j-1][i] && scores[j-1][i] > 0.0) { sc = scores[j-1][i]; }
			}
			if (USE_TSCORE && COMP_TSCORE) {
				sc = tscores[i];
			}
			for (var j = 1; j <= SUBMITS; j++) {
				var tmp = 0;
				if (SCORE_ASC_ORDER) {
					tmp = sc / scores[j-1][i];
				} else {
					tmp = scores[j-1][i] / sc;
				}
				if (isNaN(tmp) || tmp < 0.0) {
					tmp = 0;
				}
				points[j-1] += tmp;
			}
		}
		for (var j = 1; j <= SUBMITS; j++) {
			points[j-1] = 1000000.0 * points[j-1] / numOfVw;
		}
	} else {
		for (var j = 1; j <= SUBMITS; j++) {
			var sum = 0.0;
			for (var i = 0; i < numOfSc; i++) {
				var e = i + FIRST_SEED_IN_TXT;
				if (e < FIRST_SEED_OF_VIEW || LAST_SEED_OF_VIEW < e) { continue; }
				sum += scores[j-1][i];
			}
			points[j-1] = sum * 1000000.0 / numOfVw;
		}
	}
}
//
// buildRankingBoard スコアを表示するテーブルをページに埋め込む
//
function buildRankingBoard() {
	var tb = document.createElement('table');
	tb.border = 1;
	var thr = document.createElement('tr');
	var th = document.createElement('th');
	th.innerHTML = 'rank'; thr.appendChild(th);
	th = document.createElement('th');
	th.className = 'hand';
	th.title = 'sort by submit time';
	th.onclick = function() { showRanking(-1); };
	th.innerHTML = 'submit'; thr.appendChild(th);
	th = document.createElement('th');
	th.className = 'hand';
	th.title = 'sort by score';
	th.onclick = function() { showRanking(0); };
	th.innerHTML = 'score'; thr.appendChild(th);
	th = document.createElement('th');
	th.innerHTML = 'u'; thr.appendChild(th);
	th.title = 'unique count';
	th = document.createElement('th');
	th.innerHTML = 'B'; thr.appendChild(th);
	th.title = 'best count';
	th = document.createElement('th');
	th.innerHTML = 's'; thr.appendChild(th);
	th.title = 'second count';
	th = document.createElement('th');
	th.innerHTML = 't'; thr.appendChild(th);
	th.title = 'third count';
	th = document.createElement('th');
	th.innerHTML = 'b'; thr.appendChild(th);
	th.title = 'booby count';
	th = document.createElement('th');
	th.title = 'worst count';
	th.innerHTML = 'W'; thr.appendChild(th);
	th = document.createElement('th');
	th.title = 'error count';
	th.innerHTML = 'e'; thr.appendChild(th);
	for (var i = FIRST_SEED_OF_VIEW; i <= LAST_SEED_OF_VIEW; i++) {
		th = document.createElement('th');
		th.title = 'sort by seed ' + i.toString();
		th.className = 'hand';
		th.onclick = (function(tp) {
			return function() { showRanking(tp); };
		})(i);
		var inf = document.createElement('span');
		inf.appendChild(document.createTextNode('Seed=' + i.toString()));
		if (USE_INFO) {
			if (i - 1 < infos.length) {
				var sms = inf.appendChild(document.createElement('small'));
				for (var k = 0; k < infos[i - 1].length; k++) {
					sms.appendChild(document.createElement('br'));
					sms.appendChild(document.createTextNode(INFO_NAMES[k] + '=' + infos[i - 1][k]));
				}
			}
		}
		th.appendChild(inf); thr.appendChild(th);
	}
	tb.appendChild(thr);
	var tr = document.createElement('tr');
	var ap = function(tr) {
		var td = document.createElement('td');
		td.style.textAlign = 'center';
		td.innerHTML = '-'; tr.appendChild(td);
	};
	ap(tr); // rank
	ap(tr); // submit
	ap(tr); // score
	ap(tr); // unique
	ap(tr); // best
	ap(tr); // second
	ap(tr); // third
	ap(tr); // booby
	ap(tr); // worst
	ap(tr); // error
	for (var i = FIRST_SEED_OF_VIEW; i <= LAST_SEED_OF_VIEW; i++) {
		var td = document.createElement('td');
		var spn = document.createElement('span');
		td.className = 'score tscore';
		spn.innerHTML = formatFloat(tscores[i-1], SCORE_LEN);
		td.appendChild(spn);
		tr.appendChild(td);
	}
	tb.appendChild(tr);
	for (var j = 1; j <= SUBMITS; j++) {
		var tr = document.createElement('tr');
		var rk = document.createElement('td');
		var sb = document.createElement('td');
		var sc = document.createElement('td');
		var un = document.createElement('td');
		var bs = document.createElement('td');
		var sn = document.createElement('td');
		var ti = document.createElement('td');
		var bb = document.createElement('td');
		var ws = document.createElement('td');
		var er = document.createElement('td');
		var sbsp = document.createElement('span');
		var scsp = document.createElement('span');
		var unsp = document.createElement('span');
		var bssp = document.createElement('span');
		var snsp = document.createElement('span');
		var tisp = document.createElement('span');
		var bbsp = document.createElement('span');
		var wssp = document.createElement('span');
		var ersp = document.createElement('span');
		rk.style.textAlign = 'center';
		rk.innerHTML = j.toString();
		tr.appendChild(rk);
		sbsp.id = 'rbsb' + j.toString();
		sbsp.style.fontWeight = 'bold';
		sb.appendChild(sbsp);
		sb.style.textAlign = 'center';
		tr.appendChild(sb);
		scsp.id = 'rbsc' + j.toString();
		sc.appendChild(scsp);
		sc.className = 'score';
		tr.appendChild(sc);
		unsp.id = 'rbun' + j.toString();
		un.className = 'score';
		un.appendChild(unsp);
		tr.appendChild(un);
		bssp.id = 'rbbs' + j.toString();
		bs.className = 'score';
		bs.appendChild(bssp);
		tr.appendChild(bs);
		snsp.id = 'rbsn' + j.toString();
		sn.className = 'score';
		sn.appendChild(snsp);
		tr.appendChild(sn);
		tisp.id = 'rbti' + j.toString();
		ti.className = 'score';
		ti.appendChild(tisp);
		tr.appendChild(ti);
		bbsp.id = 'rbbb' + j.toString();
		bb.className = 'score';
		bb.appendChild(bbsp);
		tr.appendChild(bb);
		wssp.id = 'rbws' + j.toString();
		ws.className= 'score';
		ws.appendChild(wssp);
		tr.appendChild(ws);
		ersp.id = 'rber' + j.toString();
		er.className= 'score';
		er.appendChild(ersp);
		tr.appendChild(er);
		for (var i = FIRST_SEED_OF_VIEW; i <= LAST_SEED_OF_VIEW; i++) {
			var ex = document.createElement('td');
			var exsp = document.createElement('span');
			exsp.id = 'rbex' + j.toString() + 'no' + i.toString();
			ex.appendChild(exsp);
			ex.className = 'score';
			tr.appendChild(ex);
		}
		tb.appendChild(tr);
	}
	var swi = document.createElement('div');
	swi.onclick = function() {
		SHOW_RATE = !SHOW_RATE;
		showRanking(0);
	};
	swi.className = 'linklike hand';
	swi.appendChild(document.createTextNode('switch value/rate'));
	document.getElementById('rankboard').appendChild(swi);
	document.getElementById('rankboard').appendChild(tb);
}
//
// ignoreImageUIs 画像を使用しない場合の関連DOMパーツを非表示にする
//
function ignoreImageUIs() {
	var elms = document.getElementsByClassName('eximages');
	for (var i = 0; i < elms.length; i++) {
		elms[i].style.display = 'none';
	}
}
//
// build 画像やスコアを表示するための準備
//
function build() {
	if (USE_IMAGE) {
		document.getElementById('bigimgclose').onclick = closeBigimg;
	} else {
		ignoreImageUIs();
	}
	calcPoints();
	findSpecialScores();
	if (USE_IMAGE) {
		buildSelector();
		buildImgTable();
	}
	buildRankingBoard();
	if (USE_IMAGE) {
		showScores(DEFAULT_VIEW_SEED);
	}
	showRanking(0);
}
//
// loadScore i番目のSUBMITのスコアファイルを読み込む
//
function loadScore(i) {
	if (i > SUBMITS) {
		plog('', false);
		build();
		return;
	}
	plog('load score.txt ' + i.toString() + ' / ' + SUBMITS.toString(), false);
	xhr.onreadystatechange = function() {
		if (xhr.readyState !== 4) { return; }
		if (xhr.status !== 200) {
			SUBMITS = i - 1;
			build();
			return;
		}
		var tx = xhr.responseText.replace(/[\r\n]+/, '\n').split('\n');
		var ar = new Array();
		var j = 0;
		for (var k in tx) {
			var matches = SCORE_PARSER.exec(tx[k]);
			if (matches == null) { continue; }
			ar[j] = parseFloat(matches[1]);
			j++;
		}
		scores[i-1] = ar;
		loadScore(i+1);
	};
	xhr.open('GET', './submit'+i.toString()+'/score.txt', true);
	if (xhr.setRequestHeader) {
		xhr.setRequestHeader('Content-type', 'text/plain');
	}
	xhr.overrideMimeType = 'text/plain';
	xhr.send(null);
}
//
// 理論値スコアファイルを読み込む
//
function loadTScore() {
	plog('load TScore', false);
	xhr.onreadystatechange = function() {
		if (xhr.readyState !== 4) { return; }
		if (xhr.status !== 200) {
			USE_TSCORE = false;
			loadScore(1);
			return;
		}
		var tx = xhr.responseText.replace(/[\r\n]+/, '\n').split('\n');
		var ar = new Array();
		var j = 0;
		var u = 0, v = 0;
		for (var k in tx) {
			if (USE_INFO) {
				while (u < INFO_PARSERS.length) {
					var matches = INFO_PARSERS[u].exec(tx[k]);
					if (matches !== null) {
						if (u == 0) {
							infos[v] = new Array();
						}
						infos[v][u] = matches[1];
						u++;
					} else {
						break;
					}
				}
				if (u >= INFO_PARSERS.length) {
					u = 0;
					v++;
				}
			}
			if (USE_TSCORE) {
				var matches = TSCORE_PARSER.exec(tx[k]);
				if (matches === null) { continue; }
				tscores[j] = parseFloat(matches[1]);
				j++;
			}
		}
		loadScore(1);
	};
	xhr.open('GET', './data.txt', true);
	if (xhr.setRequestHeader) {
		xhr.setRequestHeader('Content-type', 'text/plain');
	}
	xhr.overrideMimeType = 'text/plain';
	xhr.send(null);
}
//
// getParams ページのURLのパラメータを読み込む
//
function getParams() {
	var s = location.search;
	if (typeof s !== 'string' || s.length === 0) { return; }
	var ps = s.substr(1).split('&').map(function(s){ return s.split('='); });
	var dvs = DEFAULT_VIEW_SEED;
	for (var i = 0; i < ps.length; i++) {
		if (ps[i].length !== 2) { continue; }
		if (ps[i][0] === 'all') {
			switch (("" + ps[i][1]).toLowerCase()) {
			case '0':
			case 'false':
				break;
			default:
				FIRST_SEED_OF_VIEW = FIRST_SEED_IN_TXT;
				LAST_SEED_OF_VIEW  = LAST_SEED_IN_TXT;
				break;
			}
		} else if (ps[i][0] === 'default') {
			if (("" + ps[i][1]).match(/\d+/)) {
				dvs = parseInt(ps[i][1]);
			}
		} else if (ps[i][0] === 'image') {
			switch (("" + ps[i][1]).toLowerCase()) {
			case '0':
			case 'false':
			case 'none':
			case 'off':
				USE_IMAGE = false;
				break;
			default:
				USE_IMAGE = true;
				break;
			}
		} else if (ps[i][0] === 'type') {
			switch (("" + ps[i][1]).toUpperCase()) {
			case '0':
			case 'ABS':
				SCORE_TYPE = ABS;
				break;
			case '1':
			case 'REL':
				SCORE_TYPE = REL;
				break;
			case '2':
			case 'BEAT':
				SCORE_TYPE =BEAT;
				break;
			}
		} else if (ps[i][0] === 'after') {
			switch (("" + ps[i][1]).toLowerCase()) {
			case '0':
			case 'false':
			case 'none':
			case 'off':
				SUBMITS = ONTIME_SUBMITS;
				break;
			default:
				SUBMITS = AFTER_SUBMITS;
				break;
			}
		}
	}
	if (FIRST_SEED_OF_VIEW <= dvs && dvs <= LAST_SEED_OF_VIEW) {
		DEFAULT_VIEW_SEED = dvs;
	}
}
//
// ページのロード完了時に処理を開始するよう仕込む
//
(function(f) {
	if (window.attachEvent) {
		window.attachEvent('onload', f);
	} else if (window.addEventListener) {
		window.addEventListener('load', f);
	}
})(function() {
	getParams();
	if (USE_TSCORE || USE_INFO) {
		loadTScore();
	} else {
		loadScore(1);
	}
});
})();
