Testset seed 1 to 100
________________________________________________________________
Seed ... 1 
FizzBazz!
S: 10
N: 10
W: 5
H: 16
H/S2: 0.160000
P: 1.035800
los: 0
area: 1
r,c: 4,5
sc1: 412.000000
sc2: 296.000000
time: 150 ms
Score = 412.0
________________________________________________________________
Seed ... 2 
FizzBazz!
S: 20
N: 46
W: 30
H: 21
H/S2: 0.052500
P: 0.841406
los: 0
area: 1
r,c: 7,10
sc1: 36787.000000
sc2: 26343.000000
time: 186 ms
Score = 36787.0
________________________________________________________________
Seed ... 3 
FizzBazz!
S: 40
N: 400
W: 118
H: 212
H/S2: 0.132500
P: 1.185729
los: 0
area: 1
r,c: 21,8
sc1: 74378.924647
sc2: 26213.843730
time: 250 ms
Score = 74378.92464684337
________________________________________________________________
Seed ... 4 
FizzBazz!
S: 29
N: 193
W: 48
H: 88
H/S2: 0.104637
P: 1.733925
los: 0
area: 1
r,c: 0,27
sc1: 5284.380941
sc2: 97088.711821
time: 200 ms
Score = 97088.71182057464
________________________________________________________________
Seed ... 5 
FizzBazz!
S: 12
N: 14
W: 7
H: 13
H/S2: 0.090278
P: 1.275133
los: 0
area: 1
r,c: 4,1
sc1: 209.000000
sc2: 377.000000
time: 220 ms
Score = 377.0
________________________________________________________________
Seed ... 6 
FizzBazz!
S: 27
N: 144
W: 5
H: 29
H/S2: 0.039781
P: 0.514796
los: 0
area: 1
r,c: 23,6
sc1: 26608.685134
sc2: 45377.207617
time: 190 ms
Score = 45377.20761693488
________________________________________________________________
Seed ... 7 
FizzBazz!
S: 13
N: 15
W: 6
H: 21
H/S2: 0.124260
P: 0.107822
los: 0
area: 1
r,c: 1,11
sc1: 1515.000000
sc2: 821.000000
time: 222 ms
Score = 1515.0
________________________________________________________________
Seed ... 8 
FizzBazz!
S: 31
N: 205
W: 3
H: 108
H/S2: 0.112383
P: 1.131926
los: 0
area: 1
r,c: 19,7
sc1: 21480.597897
sc2: 20884.705331
time: 277 ms
Score = 21480.597897313914
________________________________________________________________
Seed ... 9 
FizzBazz!
S: 27
N: 156
W: 43
H: 27
H/S2: 0.037037
P: 1.469377
los: 0
area: 1
r,c: 14,5
sc1: 157717.037106
sc2: 261163.027248
time: 180 ms
Score = 261163.0272479094
________________________________________________________________
Seed ... 10 
FizzBazz!
S: 35
N: 121
W: 71
H: 180
H/S2: 0.146939
P: 1.108684
los: 0
area: 1
r,c: 1,22
sc1: 36346.155906
sc2: 49905.266244
time: 203 ms
Score = 49905.26624396382
________________________________________________________________
Seed ... 11 
FizzBazz!
S: 35
N: 128
W: 24
H: 171
H/S2: 0.139592
P: 0.143769
los: 0
area: 1
r,c: 29,1
sc1: 124568.508233
sc2: 176587.150965
time: 250 ms
Score = 176587.1509647515
________________________________________________________________
Seed ... 12 
FizzBazz!
S: 38
N: 175
W: 87
H: 213
H/S2: 0.147507
P: 0.432272
los: 0
area: 1
r,c: 15,32
sc1: 1301964.655135
sc2: 1620871.799319
time: 280 ms
Score = 1620871.7993193914
________________________________________________________________
Seed ... 13 
FizzBazz!
S: 16
N: 56
W: 4
H: 1
H/S2: 0.003906
P: 0.523187
los: 0
area: 1
r,c: 11,11
sc1: 3954.635769
sc2: 35140.512779
time: 223 ms
Score = 35140.512779282195
________________________________________________________________
Seed ... 14 
FizzBazz!
S: 12
N: 16
W: 10
H: 14
H/S2: 0.097222
P: 1.789356
los: 0
area: 1
r,c: 1,0
sc1: 774.000000
sc2: 334.000000
time: 170 ms
Score = 774.0
________________________________________________________________
Seed ... 15 
FizzBazz!
S: 20
N: 65
W: 11
H: 13
H/S2: 0.032500
P: 1.506585
los: 0
area: 1
r,c: 12,19
sc1: 19473.228405
sc2: 28496.464550
time: 170 ms
Score = 28496.464549516506
________________________________________________________________
Seed ... 16 
FizzBazz!
S: 23
N: 68
W: 31
H: 38
H/S2: 0.071834
P: 1.288506
los: 0
area: 1
r,c: 2,3
sc1: 138543.000000
sc2: 136808.000000
time: 200 ms
Score = 138543.0
________________________________________________________________
Seed ... 17 
FizzBazz!
S: 36
N: 82
W: 23
H: 76
H/S2: 0.058642
P: 1.312383
los: 0
area: 1
r,c: 3,21
sc1: 336327.000000
sc2: 343244.000000
time: 260 ms
Score = 343244.0
________________________________________________________________
Seed ... 18 
FizzBazz!
S: 35
N: 185
W: 38
H: 135
H/S2: 0.110204
P: 0.510166
los: 0
area: 1
r,c: 27,15
sc1: 76663.360008
sc2: 69067.854264
time: 252 ms
Score = 76663.36000757397
________________________________________________________________
Seed ... 19 
FizzBazz!
S: 23
N: 30
W: 9
H: 77
H/S2: 0.145558
P: 0.549236
los: 0
area: 1
r,c: 11,6
sc1: 11207.000000
sc2: 8653.000000
time: 223 ms
Score = 11207.0
________________________________________________________________
Seed ... 20 
FizzBazz!
S: 10
N: 15
W: 7
H: 9
H/S2: 0.090000
P: 0.595073
los: 0
area: 1
r,c: 6,5
sc1: 798.594461
sc2: 1024.000000
time: 240 ms
Score = 1024.0
________________________________________________________________
Seed ... 21 
FizzBazz!
S: 35
N: 220
W: 34
H: 163
H/S2: 0.133061
P: 0.096089
los: 0
area: 1
r,c: 16,21
sc1: 325246.410350
sc2: 401105.397569
time: 280 ms
Score = 401105.39756892406
________________________________________________________________
Seed ... 22 
FizzBazz!
S: 26
N: 62
W: 13
H: 3
H/S2: 0.004438
P: 0.977801
los: 0
area: 1
r,c: 7,21
sc1: 152258.000000
sc2: 123155.000000
time: 200 ms
Score = 152258.0
________________________________________________________________
Seed ... 23 
FizzBazz!
S: 22
N: 33
W: 8
H: 40
H/S2: 0.082645
P: 0.836355
los: 0
area: 1
r,c: 6,11
sc1: 14418.000000
sc2: 13377.000000
time: 252 ms
Score = 14418.0
________________________________________________________________
Seed ... 24 
FizzBazz!
S: 11
N: 30
W: 1
H: 8
H/S2: 0.066116
P: 0.034247
los: 0
area: 1
r,c: 3,5
sc1: 1169.960917
sc2: 7789.989575
time: 216 ms
Score = 7789.989575185089
________________________________________________________________
Seed ... 25 
FizzBazz!
S: 31
N: 90
W: 42
H: 173
H/S2: 0.180021
P: 0.642358
los: 0
area: 2
r,c: 10,15
sc1: 24617.619827
sc2: 233028.919025
time: 220 ms
Score = 233028.91902466706
________________________________________________________________
Seed ... 26 
FizzBazz!
S: 15
N: 29
W: 7
H: 7
H/S2: 0.031111
P: 0.522249
los: 0
area: 1
r,c: 8,4
sc1: 3720.573239
sc2: 2460.870533
time: 176 ms
Score = 3720.573239300103
________________________________________________________________
Seed ... 27 
FizzBazz!
S: 20
N: 71
W: 28
H: 2
H/S2: 0.005000
P: 0.434409
los: 0
area: 1
r,c: 13,18
sc1: 48550.624279
sc2: 128059.247213
time: 250 ms
Score = 128059.24721250843
________________________________________________________________
Seed ... 28 
FizzBazz!
S: 28
N: 81
W: 36
H: 40
H/S2: 0.051020
P: 0.683069
los: 0
area: 1
r,c: 5,7
sc1: 115687.468158
sc2: 236994.000000
time: 292 ms
Score = 236994.0
________________________________________________________________
Seed ... 29 
FizzBazz!
S: 39
N: 51
W: 131
H: 32
H/S2: 0.021039
P: 0.429371
los: 0
area: 1
r,c: 8,36
sc1: 217.555865
sc2: 217.555865
time: 220 ms
Score = 217.55586507436368
________________________________________________________________
Seed ... 30 
FizzBazz!
S: 38
N: 247
W: 88
H: 223
H/S2: 0.154432
P: 0.412680
los: 0
area: 1
r,c: 21,31
sc1: 25997.203196
sc2: 3492128.041451
time: 354 ms
Score = 3492128.041450708
________________________________________________________________
Seed ... 31 
FizzBazz!
S: 10
N: 10
W: 0
H: 17
H/S2: 0.170000
P: 1.024958
los: 0
area: 1
r,c: 4,4
sc1: 281.681316
sc2: 329.000000
time: 170 ms
Score = 329.0
________________________________________________________________
Seed ... 32 
FizzBazz!
S: 22
N: 71
W: 5
H: 46
H/S2: 0.095041
P: 0.083599
los: 0
area: 1
r,c: 3,4
sc1: 66166.951999
sc2: 55236.712684
time: 190 ms
Score = 66166.95199895249
________________________________________________________________
Seed ... 33 
FizzBazz!
S: 20
N: 20
W: 26
H: 3
H/S2: 0.007500
P: 1.928021
los: 0
area: 1
r,c: 10,14
sc1: 1243.000000
Score = 2228.0
sc2: 2228.000000
time: 200 ms
________________________________________________________________
Seed ... 34 
FizzBazz!
S: 10
N: 10
W: 2
H: 2
H/S2: 0.020000
P: 0.612482
los: 0
area: 1
r,c: 7,8
sc1: 142.000000
sc2: 70.000000
time: 200 ms
Score = 142.0
________________________________________________________________
Seed ... 35 
FizzBazz!
S: 19
N: 40
W: 31
H: 30
H/S2: 0.083102
P: 1.846884
los: 0
area: 1
r,c: 11,7
sc1: 11642.000000
sc2: 7361.000000
time: 210 ms
Score = 11642.0
________________________________________________________________
Seed ... 36 
FizzBazz!
S: 39
N: 160
W: 37
H: 90
H/S2: 0.059172
P: 1.004391
los: 0
area: 1
r,c: 34,10
sc1: 556744.958790
sc2: 1149891.218851
time: 380 ms
Score = 1149891.218850958
________________________________________________________________
Seed ... 37 
FizzBazz!
S: 14
N: 31
W: 11
H: 19
H/S2: 0.096939
P: 0.994626
los: 0
area: 1
r,c: 7,9
sc1: 8590.633620
sc2: 8196.000000
time: 195 ms
Score = 8590.633619631095
________________________________________________________________
Seed ... 38 
FizzBazz!
S: 27
N: 29
W: 17
H: 127
H/S2: 0.174211
P: 1.132490
los: 0
area: 1
r,c: 17,11
sc1: 7733.000000
sc2: 9355.000000Score = 9355.0

time: 268 ms
________________________________________________________________
Seed ... 39 
FizzBazz!
S: 13
N: 25
W: 0
H: 4
H/S2: 0.023669
P: 0.955467
los: 0
area: 1
r,c: 9,7
sc1: 5871.000000
sc2: 6526.000000
time: 285 ms
Score = 6526.0
________________________________________________________________
Seed ... 40 
FizzBazz!
S: 38
N: 232
W: 114
H: 224
H/S2: 0.155125
P: 1.917441
los: 0
area: 1
r,c: 16,13
sc1: 60835.639107
sc2: 927904.489036
time: 211 ms
Score = 927904.4890360847
________________________________________________________________
Seed ... 41 
FizzBazz!
S: 21
N: 103
W: 2
H: 53
H/S2: 0.120181
P: 0.701554
los: 0
area: 1
r,c: 10,9
sc1: 10496.681369
sc2: 24928.003581
time: 168 ms
Score = 24928.003581444016
________________________________________________________________
Seed ... 42 
FizzBazz!
S: 26
N: 81
W: 56
H: 101
H/S2: 0.149408
P: 1.174789
los: 0
area: 2
r,c: 6,17
sc1: 466.273043
sc2: 19720.154642
time: 160 ms
Score = 19720.15464219957
________________________________________________________________
Seed ... 43 
FizzBazz!
S: 16
N: 49
W: 13
H: 27
H/S2: 0.105469
P: 0.065559
los: 0
area: 1
r,c: 3,3
sc1: 4187.155743
sc2: 5067.157907
time: 256 ms
Score = 5067.157907479308
________________________________________________________________
Seed ... 44 
FizzBazz!
S: 29
N: 183
W: 5
H: 110
H/S2: 0.130797
P: 0.500745
los: 0
area: 1
r,c: 9,24
sc1: 16718.766789
sc2: 9254.364210
time: 247 ms
Score = 16718.766788831126
________________________________________________________________
Seed ... 45 
FizzBazz!
S: 22
N: 99
W: 27
H: 4
H/S2: 0.008264
P: 0.760580
los: 0
area: 1
r,c: 16,9
sc1: 129161.492133
sc2: 84087.954026
time: 201 ms
Score = 129161.49213344425
________________________________________________________________
Seed ... 46 
FizzBazz!
S: 36
N: 101
W: 8
H: 44
H/S2: 0.033951
P: 1.207722
los: 0
area: 1
r,c: 4,10
sc1: 88808.948122
sc2: 709539.000000
time: 269 ms
Score = 709539.0
________________________________________________________________
Seed ... 47 
FizzBazz!
S: 17
N: 70
W: 7
H: 22
H/S2: 0.076125
P: 0.044290
los: 0
area: 1
r,c: 12,14
Score = 19263.721232698757
sc1: 19263.721233
sc2: 18732.356127
time: 214 ms
________________________________________________________________
Seed ... 48 
FizzBazz!
S: 33
N: 40
W: 58
H: 24
H/S2: 0.022039
P: 1.454805
los: 0
area: 1
r,c: 8,13
sc1: 14007.000000
sc2: 11684.000000
time: 336 ms
Score = 14007.0
________________________________________________________________
Seed ... 49 
FizzBazz!
S: 19
N: 37
W: 25
H: 31
H/S2: 0.085873
P: 0.917583
los: 0
area: 1
r,c: 8,9
sc1: 17704.000000
sc2: 10783.000000
time: 170 ms
Score = 17704.0
________________________________________________________________
Seed ... 50 
FizzBazz!
S: 21
N: 102
W: 16
H: 49
H/S2: 0.111111
P: 0.769121
los: 0
area: 1
r,c: 18,6
sc1: 9818.509208
sc2: 73366.826939
time: 180 ms
Score = 73366.82693948425
________________________________________________________________
Seed ... 51 
FizzBazz!
S: 28
N: 181
W: 43
H: 1
H/S2: 0.001276
P: 0.754843
los: 0
area: 1
r,c: 16,27
sc1: 356405.309216
sc2: 474218.743211
time: 230 ms
Score = 474218.74321110285
________________________________________________________________
Seed ... 52 
FizzBazz!
S: 26
N: 35
W: 50
H: 47
H/S2: 0.069527
P: 1.327024
los: 0
area: 1
r,c: 0,2
sc1: 14988.000000
sc2: 10686.000000
time: 190 ms
Score = 14988.0
________________________________________________________________
Seed ... 53 
FizzBazz!
S: 34
N: 117
W: 98
H: 63
H/S2: 0.054498
P: 0.015683
los: 0
area: 1
r,c: 17,13
sc1: 485997.000000
sc2: 329788.000000
time: 401 ms
Score = 485997.0
________________________________________________________________
Seed ... 54 
FizzBazz!
S: 22
N: 94
W: 12
H: 5
H/S2: 0.010331
P: 0.732815
los: 0
area: 1
r,c: 18,10
sc1: 82078.360472
sc2: 95854.407960
time: 252 ms
Score = 95854.40795972296
________________________________________________________________
Seed ... 55 
FizzBazz!
S: 21
N: 62
W: 35
H: 12
H/S2: 0.027211
P: 1.562511
los: 0
area: 1
r,c: 16,14
sc1: 86935.000000
sc2: 62083.000000
time: 342 ms
Score = 86935.0
________________________________________________________________
Seed ... 56 
FizzBazz!
S: 26
N: 159
W: 38
H: 14
H/S2: 0.020710
P: 1.036249
los: 0
area: 1
r,c: 6,3
sc1: 116703.627878
sc2: 210577.602088
time: 345 ms
Score = 210577.60208838113
________________________________________________________________
Seed ... 57 
FizzBazz!
S: 30
N: 161
W: 0
H: 0
H/S2: 0.000000
P: 1.146301
los: 0
area: 1
r,c: 25,2
sc1: 640.095745
sc2: 119841.262645
time: 263 ms
Score = 119841.26264528773
________________________________________________________________
Seed ... 58 
FizzBazz!
S: 12
N: 36
W: 1
H: 2
H/S2: 0.013889
P: 1.551575
los: 0
area: 1
r,c: 8,8
sc1: 4152.702014
sc2: 3916.169909
time: 216 ms
Score = 4152.702014351144
________________________________________________________________
Seed ... 59 
FizzBazz!
S: 15
N: 18
W: 0
H: 39
H/S2: 0.173333
P: 1.127872
los: 0
area: 1
r,c: 0,4
sc1: 535.582956
sc2: 2212.000000
time: 210 ms
Score = 2212.0
________________________________________________________________
Seed ... 60 
FizzBazz!
S: 32
N: 135
W: 23
H: 104
H/S2: 0.101563
P: 0.723398
los: 0
area: 1
r,c: 20,2
sc1: 130737.805627
sc2: 585238.702473
time: 316 ms
Score = 585238.7024726695
________________________________________________________________
Seed ... 61 
FizzBazz!
S: 30
N: 108
W: 14
H: 45
H/S2: 0.050000
P: 0.754420
los: 0
area: 1
r,c: 1,5
sc1: 146838.475586
sc2: 214721.249895
time: 289 ms
Score = 214721.24989529597
________________________________________________________________
Seed ... 62 
FizzBazz!
S: 24
N: 126
W: 26
H: 68
H/S2: 0.118056
P: 0.285865
los: 0
area: 1
r,c: 23,12
sc1: 142335.523439
sc2: 147060.372504
time: 290 ms
Score = 147060.37250428795
________________________________________________________________
Seed ... 63 
FizzBazz!
S: 40
N: 77
W: 78
H: 256
H/S2: 0.160000
P: 0.552269
los: 0
area: 4
r,c: 22,0
sc1: 71546.005352
sc2: 154106.000000
time: 196 ms
Score = 154106.0
________________________________________________________________
Seed ... 64 
FizzBazz!
S: 38
N: 166
W: 73
H: 0
H/S2: 0.000000
P: 1.761184
los: 0
area: 1
r,c: 4,33
sc1: 221503.495437
sc2: 2567281.068881
time: 230 ms
Score = 2567281.0688807894
________________________________________________________________
Seed ... 65 
FizzBazz!
S: 36
N: 169
W: 45
H: 16
H/S2: 0.012346
P: 1.750641
los: 0
area: 1
r,c: 2,30
sc1: 72209.852085
sc2: 1033766.270172
time: 233 ms
Score = 1033766.2701723366
________________________________________________________________
Seed ... 66 
FizzBazz!
S: 28
N: 127
W: 17
H: 62
H/S2: 0.079082
P: 1.676577
los: 0
area: 1
r,c: 21,17
sc1: 9480.720960
sc2: 27220.586707
time: 180 ms
Score = 27220.586707438993
________________________________________________________________
Seed ... 67 
FizzBazz!
S: 39
N: 149
W: 99
H: 205
H/S2: 0.134780
P: 1.794915
los: 0
area: 1
r,c: 14,27
sc1: 210389.769757
sc2: 1650182.000000
time: 226 ms
Score = 1650182.0
________________________________________________________________
Seed ... 68 
FizzBazz!
S: 19
N: 56
W: 4
H: 3
H/S2: 0.008310
P: 1.047880
los: 0
area: 1
r,c: 10,12
sc1: 43576.304306
sc2: 109167.000000
time: 220 ms
Score = 109167.0
________________________________________________________________
Seed ... 69 
FizzBazz!
S: 11
N: 26
W: 1
H: 10
H/S2: 0.082645
P: 0.543629
los: 0
area: 1
r,c: 2,2
sc1: 2805.324630
sc2: 4791.242749
time: 263 ms
Score = 4791.242748536477
________________________________________________________________
Seed ... 70 
FizzBazz!
S: 37
N: 103
W: 98
H: 24
H/S2: 0.017531
P: 1.755843
los: 0
area: 1
r,c: 36,31
sc1: 276263.000000
sc2: 366924.000000
time: 286 ms
Score = 366924.0
________________________________________________________________
Seed ... 71 
FizzBazz!
S: 19
N: 67
W: 24
H: 36
H/S2: 0.099723
P: 0.326782
los: 0
area: 1
r,c: 11,17
sc1: 56629.187776
sc2: 6479.084004
time: 242 ms
Score = 56629.18777599693
________________________________________________________________
Seed ... 72 
FizzBazz!
S: 31
N: 57
W: 60
H: 88
H/S2: 0.091571
P: 1.869688
los: 0
area: 1
r,c: 12,18
sc1: 80285.000000
sc2: 56296.000000
time: 239 ms
Score = 80285.0
________________________________________________________________
Seed ... 73 
FizzBazz!
S: 35
N: 102
W: 63
H: 11
H/S2: 0.008980
P: 1.274702
los: 0
area: 1
r,c: 11,27
sc1: 67037.031146
sc2: 363140.000000
time: 286 ms
Score = 363140.0
________________________________________________________________
Seed ... 74 
FizzBazz!
S: 38
N: 140
W: 47
H: 60
H/S2: 0.041551
P: 0.531392
los: 0
area: 1
r,c: 31,24
sc1: 804321.103640
sc2: 2049163.000000
time: 300 ms
Score = 2049163.0
________________________________________________________________
Seed ... 75 
FizzBazz!
S: 28
N: 140
W: 32
H: 23
H/S2: 0.029337
P: 0.106719
los: 0
area: 1
r,c: 10,23
sc1: 589360.675806
sc2: 947475.491274
time: 393 ms
Score = 947475.4912738518
________________________________________________________________
Seed ... 76 
FizzBazz!
S: 40
N: 150
W: 107
H: 61
H/S2: 0.038125
P: 1.681015
los: 0
area: 1
r,c: 18,1
sc1: 1357464.000000
sc2: 1174106.000000
time: 375 ms
Score = 1357464.0
________________________________________________________________
Seed ... 77 
FizzBazz!
S: 40
N: 387
W: 114
H: 21
H/S2: 0.013125
P: 1.074463
los: 0
area: 1
r,c: 18,15
sc1: 2539617.162601
sc2: 1540277.079068
time: 271 ms
Score = 2539617.162601164
________________________________________________________________
Seed ... 78 
FizzBazz!
S: 14
N: 24
W: 15
H: 27
H/S2: 0.137755
P: 0.679488
los: 0
area: 1
r,c: 8,6
sc1: 3515.000000
sc2: 1368.655908
time: 271 ms
Score = 3515.0
________________________________________________________________
Seed ... 79 
FizzBazz!
S: 30
N: 196
W: 5
H: 36
H/S2: 0.040000
P: 0.713792
los: 0
area: 1
r,c: 22,10
sc1: 39524.207617
sc2: 71460.833487
time: 398 ms
Score = 71460.83348748725
________________________________________________________________
Seed ... 80 
FizzBazz!
S: 15
N: 23
W: 0
H: 31
H/S2: 0.137778
P: 0.887169
los: 0
area: 1
r,c: 3,1
sc1: 1965.592474
sc2: 2365.000000
time: 213 ms
Score = 2365.0
________________________________________________________________
Seed ... 81 
FizzBazz!
S: 24
N: 129
W: 18
H: 9
H/S2: 0.015625
P: 1.265144
los: 0
area: 1
r,c: 1,14
sc1: 24974.900931
sc2: 93995.941405
time: 258 ms
Score = 93995.94140534937
________________________________________________________________
Seed ... 82 
FizzBazz!
S: 32
N: 55
W: 81
H: 30
H/S2: 0.029297
P: 1.402500
los: 0
area: 1
r,c: 18,29
sc1: 45442.000000
sc2: 26679.000000
time: 230 ms
Score = 45442.0
________________________________________________________________
Seed ... 83 
FizzBazz!
S: 20
N: 47
W: 26
H: 36
H/S2: 0.090000
P: 1.195076
los: 0
area: 1
r,c: 0,7
sc1: 40552.000000
sc2: 138.882945
time: 187 ms
Score = 40552.0
________________________________________________________________
Seed ... 84 
FizzBazz!
S: 35
N: 93
W: 99
H: 215
H/S2: 0.175510
P: 1.627959
los: 0
area: 3
r,c: 19,20
sc1: 264840.000000
sc2: 9949.900608
time: 190 ms
Score = 264840.0
________________________________________________________________
Seed ... 85 
FizzBazz!
S: 21
N: 25
W: 29
H: 21
H/S2: 0.047619
P: 0.520072
los: 0
area: 1
r,c: 12,7
sc1: 4639.000000
sc2: 3264.000000
time: 249 ms
Score = 4639.0
________________________________________________________________
Seed ... 86 
FizzBazz!
S: 13
N: 40
W: 7
H: 22
H/S2: 0.130178
P: 1.946585
los: 0
area: 1
r,c: 5,6
sc1: 613.837095
sc2: 909.732354
time: 230 ms
Score = 909.7323538715879
________________________________________________________________
Seed ... 87 
FizzBazz!
S: 25
N: 62
W: 41
H: 85
H/S2: 0.136000
P: 0.373271
los: 0
area: 1
r,c: 3,21
sc1: 133962.000000
sc2: 85059.000000
time: 237 ms
Score = 133962.0
________________________________________________________________
Seed ... 88 
FizzBazz!
S: 23
N: 97
W: 27
H: 27
H/S2: 0.051040
P: 1.288477
los: 0
area: 1
r,c: 3,15
sc1: 70633.286530
sc2: 29777.593000
time: 170 ms
Score = 70633.28653041173
________________________________________________________________
Seed ... 89 
FizzBazz!
S: 36
N: 270
W: 20
H: 36
H/S2: 0.027778
P: 0.069943
los: 0
area: 1
r,c: 0,8
sc1: 1768.965439
sc2: 741136.987687
time: 240 ms
Score = 741136.9876866793
________________________________________________________________
Seed ... 90 
FizzBazz!
S: 12
N: 19
W: 1
H: 2
H/S2: 0.013889
P: 0.686786
los: 0
area: 1
r,c: 11,1
sc1: 1427.000000
sc2: 1601.000000
time: 200 ms
Score = 1601.0
________________________________________________________________
Seed ... 91 
FizzBazz!
S: 20
N: 22
W: 31
H: 21
H/S2: 0.052500
P: 1.151802
los: 0
area: 1
r,c: 15,17
sc1: 287.000000
sc2: 935.000000
time: 190 ms
Score = 935.0
________________________________________________________________
Seed ... 92 
FizzBazz!
S: 39
N: 267
W: 108
H: 85
H/S2: 0.055884
P: 0.816705
los: 0
area: 1
r,c: 38,16
sc1: 616335.902561
sc2: 3978097.269298
time: 240 ms
Score = 3978097.2692982815
________________________________________________________________
Seed ... 93 
FizzBazz!
S: 33
N: 163
W: 21
H: 161
H/S2: 0.147842
P: 1.689894
los: 0
area: 1
r,c: 24,2
sc1: 20806.927078
sc2: 16770.881685
time: 220 ms
Score = 20806.927078154997
________________________________________________________________
Seed ... 94 
FizzBazz!
S: 36
N: 193
W: 32
H: 143
H/S2: 0.110340
P: 1.962199
los: 0
area: 2
r,c: 29,6
sc1: 4883.811504
sc2: 105139.820790
time: 240 ms
Score = 105139.8207897386
________________________________________________________________
Seed ... 95 
FizzBazz!
S: 31
N: 194
W: 50
H: 11
H/S2: 0.011446
P: 0.344617
los: 0
area: 1
r,c: 6,25
sc1: 1122359.910472
sc2: 1130344.894108
time: 260 ms
Score = 1130344.8941076577
________________________________________________________________
Seed ... 96 
FizzBazz!
S: 26
N: 148
W: 3
H: 76
H/S2: 0.112426
P: 1.209524
los: 0
area: 1
r,c: 6,24
sc1: 13146.632918
sc2: 9518.734764
time: 220 ms
Score = 13146.632918189338
________________________________________________________________
Seed ... 97 
FizzBazz!
S: 16
N: 16
W: 2
H: 17
H/S2: 0.066406
P: 1.551913
los: 0
area: 1
r,c: 8,8
sc1: 1005.000000
sc2: 831.000000
time: 160 ms
Score = 1005.0
________________________________________________________________
Seed ... 98 
FizzBazz!
S: 30
N: 126
W: 58
H: 36
H/S2: 0.040000
P: 0.191314
los: 0
area: 1
r,c: 0,25
sc1: 979764.174000
sc2: 1005622.960909
time: 210 ms
Score = 1005622.960908636
________________________________________________________________
Seed ... 99 
FizzBazz!
S: 11
N: 14
W: 9
H: 10
H/S2: 0.082645
P: 0.815798
los: 0
area: 1
r,c: 4,4
sc1: 534.000000
sc2: 929.000000
time: 200 ms
Score = 929.0
________________________________________________________________
Seed ... 100 
FizzBazz!
S: 34
N: 174
W: 33
H: 129
H/S2: 0.111592
P: 0.663265
los: 0
area: 1
r,c: 14,16
sc1: 375538.522995
sc2: 629118.972271
time: 220 ms
Score = 629118.9722709459
