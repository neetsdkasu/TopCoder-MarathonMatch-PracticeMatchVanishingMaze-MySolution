import java.io.*;
import java.util.*;

public class VanishingMaze {

    public static void main(String[] args) throws Exception {
        Main.main(args);
    }

    static final int[] dt = {1, 0, -1, 0, 1};
    static final char[] cmd = {'D', 'L', 'U', 'R'};
    static final byte[] cmdB = {'D', 'L', 'U', 'R'};

    static final byte[] cmdB5 = {'D', 'U', 'L', 'R'};
    static final int[] dt5 = {1, 0, -1, 0, 0, -1, 0, 1};

    long timeLimit = 0L;

    int[] numbers;
    int S, N = 0;
    int playerRow;
    int playerCol;
    int maxSteps;
    double power;

    public String getPath(int[] numbers, int playerRow, int playerCol, double power) {
        timeLimit = System.currentTimeMillis() + 9000L;
        this.numbers = numbers;
        S = (int)Math.sqrt((double)numbers.length);
        this.playerRow = playerRow;
        this.playerCol = playerCol;
        this.power = power;
        analyze();

        Result res1 = solve1();
        Result res2 = solve2();
        System.err.printf("sc1: %f%n", res1.getScore());
        System.err.printf("sc2: %f%n", res2.getScore());
        Result res = res1.getScore() > res2.getScore() ? res1 : res2;

        // Result res3 = solve3();
        // System.err.printf("sc3: %f%n", res3.getScore());
        // if (res3.getScore() != res.getScore()) {
            // res = res3;
        // }

        Result res4 = solve4();
        System.err.printf("sc4: %f%n", res4.getScore());
        // if (res4.getScore() != res.getScore()) {
            res = res4;
        // }

        // Result res5 = solve5();
        // System.err.printf("sc5: %f%n", res5.getScore());
        // if (res5.getScore() != res.getScore()) {
            // res = res5;
        // }

        return res.getCommand();
    }

    private void analyze() {
        int[] visited = new int[numbers.length];
        int[] stack = new int[numbers.length];
        int area = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == 0) { continue; }
            if (visited[i] > 0) { continue; }
            area++;
            visited[i] = area;
            int sp = 1;
            stack[0] = i;
            while (sp > 0) {
                sp--;
                int p = stack[sp];
                for (int di = 0; di < 4; di++) {
                    int x = ((p % S) + dt[di] + S) % S;
                    int y = ((p / S) + dt[di + 1] + S) % S;
                    int nx = y * S + x;
                    if (numbers[nx] == 0) { continue; }
                    if (visited[nx] > 0) { continue; }
                    visited[nx] = area;
                    stack[sp] = nx;
                    sp++;
                }
            }
        }
        int pa = visited[playerRow * S + playerCol];
        int N = 0, W = 0;
        int[] cnts = new int[S * S / 4 + 1];
        for (int i = 0; i < numbers.length; i++) {
            int v = numbers[i];
            this.N = Math.max(this.N, v);
            if (visited[i] != pa) {
                cnts[0]++;
                continue;
            }
            N = Math.max(N, v);
            if (v < 0) {
                W++;
            } else {
                cnts[v]++;
            }
        }
        int los = 0;
        for (int i = 1; i <= N; i++) {
            if (cnts[i] == 0) {
                los++;
            }
        }
        maxSteps = 4 * N * S;
        System.err.printf("S: %d%n", S);
        System.err.printf("N: %d%n", N);
        System.err.printf("W: %d%n", W);
        System.err.printf("H: %d%n", cnts[0]);
        System.err.printf("H/S2: %f%n", (double)cnts[0] / (double)(numbers.length));
        System.err.printf("P: %f%n", power);
        System.err.printf("los: %d%n", los);
        System.err.printf("area: %d%n", area);
        System.err.printf("r,c: %d,%d%n", playerCol,playerRow);
        System.err.printf("step: %d%n", maxSteps);
    }

    private Result solve1() {
        int[] numbers = (int[])this.numbers.clone();
        int playerRow = this.playerRow;
        int playerCol = this.playerCol;
        long tmpScore = 0;
        StringBuilder ret = new StringBuilder();

        int nextTarget = 1;
        IntList tmp1 = new IntList(), tmp2 = new IntList();
        for (;;) {
            StringBuilder sb = new StringBuilder();
            int[] distance = new int[numbers.length];
            tmp1.clear();
            tmp1.add(playerRow * S + playerCol);
            distance[playerRow * S + playerCol] = 1;
            int pos = -1;
        bfs:
            while (!tmp1.isEmpty()) {
                tmp2.clear();
                for (int i = 0; i < tmp1.size(); i++) {
                    int p = tmp1.get(i);
                    int d = distance[p] + 1;
                    int py = p / S, px = p % S;
                    for (int j = 0; j < 4; j++) {
                        int y = py + dt[j];
                        int x = px + dt[j + 1];
                        if (!inField(x, y)) {
                            continue;
                        }
                        int e = y * S + x;
                        int w = distance[e];
                        if (w != 0 && w <= d) {
                            continue;
                        }
                        int n = numbers[e];
                        if (n == 0) {
                            continue;
                        }
                        distance[e] = d;
                        if (n == nextTarget || n < 0) {
                            pos = e;
                            break bfs;
                        }
                        tmp2.add(e);
                    }
                }
                IntList tmp3 = tmp1; tmp1 = tmp2; tmp2 = tmp3;
            }
            if (pos < 0) {
                break;
            }
            numbers[pos] = nextTarget;
            int d = distance[pos];
            int py = pos / S, px = pos % S;
            while (py != playerRow || px != playerCol) {
                boolean ok = false;
                for (int j = 0; j < 4; j++) {
                    int y = py + dt[j];
                    int x = px + dt[j + 1];
                    if (!inField(x, y)) {
                        continue;
                    }
                    int e = y * S + x;
                    int w = distance[e];
                    if (w > 0 && w < d) {
                        if (numbers[e] > nextTarget) {
                            tmpScore += (long)(numbers[e] * nextTarget);
                            numbers[e] = 0;
                        }
                        sb.append(cmd[(j + 2) & 3]);
                        py = y; px = x; d = w;
                        ok = true;
                        break;
                    }
                }
                if (!ok) {
                    throw new RuntimeException("what?");
                }
            }
            if (nextTarget == 1 && tmpScore > 0L) {
                tmpScore -= (long)this.numbers[this.playerRow * S + this.playerCol];
            }
            ret.append(sb.reverse());
            nextTarget++;
            playerRow = pos / S;
            playerCol = pos % S;
            if (nextTarget > N) {
                break;
            }
        }
        System.err.printf("solve1end: %d/ %d%n", nextTarget - 1, N);
        double score = (double)tmpScore * Math.pow((double)(nextTarget - 1) / (double)N, power);
        return new Result(ret.toString(), score);
    }

    private Result solve2() {
        int[] numbers = (int[])this.numbers.clone();
        int playerRow = this.playerRow;
        int playerCol = this.playerCol;
        long tmpScore = 0;
        StringBuilder ret = new StringBuilder();

        int nextTarget = 1;
        IntList tmp1 = new IntList(), tmp2 = new IntList();
        for (;;) {
            StringBuilder sb = new StringBuilder();
            int[] distance = new int[numbers.length];
            tmp1.clear();
            tmp1.add(playerRow * S + playerCol);
            distance[playerRow * S + playerCol] = 1;
            int pos = -1;
        bfs:
            while (!tmp1.isEmpty()) {
                tmp2.clear();
                for (int i = 0; i < tmp1.size(); i++) {
                    int p = tmp1.get(i);
                    int d = distance[p] + 1;
                    int py = p / S, px = p % S;
                    for (int j = 0; j < 4; j++) {
                        int y = (py + dt[j] + S) % S;
                        int x = (px + dt[j + 1] + S) % S;
                        if (!inField(x, y)) {
                            continue;
                        }
                        int e = y * S + x;
                        int w = distance[e];
                        if (w != 0 && w <= d) {
                            continue;
                        }
                        int n = numbers[e];
                        if (n == 0) {
                            continue;
                        }
                        distance[e] = d;
                        if (n == nextTarget || n < 0) {
                            pos = e;
                            break bfs;
                        }
                        tmp2.add(e);
                    }
                }
                IntList tmp3 = tmp1; tmp1 = tmp2; tmp2 = tmp3;
            }
            if (pos < 0) {
                break;
            }
            numbers[pos] = nextTarget;
            int d = distance[pos];
            int py = pos / S, px = pos % S;
            while (py != playerRow || px != playerCol) {
                boolean ok = false;
                for (int j = 0; j < 4; j++) {
                    int y = (py + dt[j] + S) % S;
                    int x = (px + dt[j + 1] + S) % S;
                    if (!inField(x, y)) {
                        continue;
                    }
                    int e = y * S + x;
                    int w = distance[e];
                    if (w > 0 && w < d) {
                        if (numbers[e] > nextTarget) {
                            tmpScore += (long)(numbers[e] * nextTarget);
                            numbers[e] = 0;
                        }
                        sb.append(cmd[(j + 2) & 3]);
                        py = y; px = x; d = w;
                        ok = true;
                        break;
                    }
                }
                if (!ok) {
                    throw new RuntimeException("what?");
                }
            }
            if (nextTarget == 1 && tmpScore > 0L) {
                tmpScore -= (long)this.numbers[this.playerRow * S + this.playerCol];
            }
            ret.append(sb.reverse());
            nextTarget++;
            playerRow = pos / S;
            playerCol = pos % S;
            if (nextTarget > N) {
                break;
            }
        }
        System.err.printf("solve2end: %d/ %d%n", nextTarget - 1, N);
        double score = (double)tmpScore * Math.pow((double)(nextTarget - 1) / (double)N, power);
        return new Result(ret.toString(), score);
    }

    boolean inField(int row, int col) {
        return 0 <= row && row < S && 0 <= col && col < S;
    }

    void fill1(int playerRow, int playerCol) {
        for (int y = 0; y < S; y++) {
            for (int x = 0; x < S; x++) {
                int cnt = 0;
                for (int j = 0; j < 4; j++) {
                    int row = y + dt[j];
                    int col = x + dt[j + 1];
                    if (playerRow == row && playerCol == col) {
                        continue;
                    }
                    if (!inField(row, col) || numbers[row * S + col] == 0) {
                        cnt++;
                    }
                }
                if (cnt == 3) {
                    numbers[y * S + x] = -3;
                }
            }
        }
    }

    void fill2(int playerRow, int playerCol) {
        for (int y = 0; y < S; y++) {
            for (int x = 0; x < S; x++) {
                int cnt = 0;
                for (int j = 0; j < 4; j++) {
                    int row = (y + dt[j] + S) % S;
                    int col = (x + dt[j + 1] + S) % S;
                    if (playerRow == row && playerCol == col) {
                        continue;
                    }
                    if (!inField(row, col) || numbers[row * S + col] == 0) {
                        cnt++;
                    }
                }
                if (cnt == 3) {
                    numbers[y * S + x] = -3;
                }
            }
        }
    }

    static final int pack(int nextTarget, int rev, int row, int col, int di) {
        return
            (nextTarget << (9 + 6 + 6 + 2))
            | (rev << (6 + 6 + 2))
            | (row << (6 + 2))
            | (col << 2)
            | di;
    }
    static final int depackNT(int p) {
        return p >> (9 + 6 + 6 + 2);
    }
    static final int depackRev(int p) {
        return (p >> (6 + 6 + 2)) & ((1 << 9) - 1);
    }
    static final int depackRow(int p) {
        return (p >> (6 + 2)) & ((1 << 6) - 1);
    }
    static final int depackCol(int p) {
        return (p >> 2) & ((1 << 6) - 1);
    }
    static final int depackDi(int p) {
        return p & ((1 << 2) - 1);
    }

    final int move(int v, int d) {
        return (v + d + S) % S;
    }

    final int ind(int row, int col) {
        return row * S + col;
    }

    private Result solve3() {

        IntStack stack = new IntStack(maxSteps * 4 + 100);

        for (int di = 0; di < 4; di++) {
            int row = move(playerRow, dt[di]);
            int col = move(playerCol, dt[di + 1]);
            if (numbers[ind(row, col)] != 0) {
                stack.push(pack(1, 0, row, col, di));
            }
        }

        double bestScore = 0;
        byte[] bestSteps = new byte[maxSteps];
        int bestStepLen = 0;

        int[] state = new int[numbers.length];

        long tmpScore = 0;
        byte[] steps = new byte[maxSteps];
        int stepi = 0;

        long time0 = System.currentTimeMillis();
        long time1 = time0;
        long interval = 0L;

        while (!stack.isEmpty()) {
            time0 = time1;
            time1 = System.currentTimeMillis();
            interval = Math.max(interval, time1 - time0);
            if (time1 + interval > timeLimit) {
                break;
            }
            int p = stack.pop();
            int nextTarget = depackNT(p);
            int rev = depackRev(p);
            int row0 = depackRow(p);
            int col0 = depackCol(p);
            int di0 = depackDi(p);
            int pos0 = ind(row0, col0);
            int num0 = numbers[pos0];

            if (rev != 0) {
                if (num0 > nextTarget) {
                    tmpScore -= (long)(num0 * nextTarget);
                }
                state[pos0] = rev-1;
                stepi--;
                continue;
            }

            stack.push(pack(nextTarget, state[pos0]+1, row0, col0, di0));

            steps[stepi] = cmdB[di0];
            stepi++;

            if (num0 > nextTarget) {
                tmpScore += (long)(num0 * nextTarget);
            } else if (num0 == nextTarget || (num0 < 0 && state[pos0] == 0)) {
                // calcScore and update solution
                double score = (double)tmpScore * Math.pow((double)nextTarget / (double)N, power);
                if (score > bestScore) {
                    bestStepLen = stepi;
                    System.arraycopy(steps, 0, bestSteps, 0, bestStepLen);
                    bestScore = score;
                }
                nextTarget++;
                if (nextTarget > N) {
                    continue;
                }
            }
            if (stepi == maxSteps) {
                continue;
            }
            state[pos0] = nextTarget;

            int[] tmp = new int[4];
            int tc = 0;

            for (int di = 0; di < 4; di++) {
                int row = move(row0, dt[di]);
                int col = move(col0, dt[di + 1]);
                int pos = ind(row, col);
                if (numbers[pos] == 0) { continue; }
                if (state[pos] == nextTarget) { continue; }
                if (state[pos] > 0 && state[pos] < numbers[pos]) { continue; }

                int dr = Math.abs(playerRow - row);
                // dr = Math.min(dr, Math.abs(playerRow - (row + S)));
                // dr = Math.min(dr, Math.abs(playerRow - (row - S)));
                int dc = Math.abs(playerCol - col);
                // dc = Math.min(dc, Math.abs(playerCol - (col + S)));
                // dc = Math.min(dc, Math.abs(playerCol - (col - S)));
                tmp[tc] = (state[pos] == 0 ? 0 : (1 << (6 + 6 + 2)))
                    | (Math.max(dr, dc) << (6 + 2))
                    | (Math.min(dr, dc) << 2)
                    | di;
                tc++;
            }

            Arrays.sort(tmp, 0, tc);
            for (int i = tc - 1; i >= 0; i--) {
                int di = tmp[i] & 3;
                int row = move(row0, dt[di]);
                int col = move(col0, dt[di + 1]);
                stack.push(pack(nextTarget, 0, row, col, di));
            }
        }
        System.err.printf("len: %d/ %d%n", bestStepLen, maxSteps);
        return new Result(new String(bestSteps, 0, bestStepLen), bestScore);
    }


    private Result solve4() {

        IntStack stack = new IntStack(maxSteps * 4 + 100);
        IntStack scstack = new IntStack(maxSteps);

        for (int di = 0; di < 4; di++) {
            int row = move(playerRow, dt[di]);
            int col = move(playerCol, dt[di + 1]);
            if (numbers[ind(row, col)] != 0) {
                stack.push(pack(1, 0, row, col, di));
            }
        }

        double bestScore = 0;
        byte[] bestSteps = new byte[maxSteps];
        int bestStepLen = 0;

        int[] state = new int[numbers.length];
        int[] scstate = new int[numbers.length];

        int tmpScore = 0;
        byte[] steps = new byte[maxSteps];
        int stepi = 0;

        long time0 = System.currentTimeMillis();
        long time1 = time0;
        long interval = 0L;

        while (!stack.isEmpty()) {
            time0 = time1;
            time1 = System.currentTimeMillis();
            interval = Math.max(interval, time1 - time0);
            if (time1 + interval > timeLimit) {
                break;
            }
            int p = stack.pop();
            int nextTarget = depackNT(p);
            int rev = depackRev(p);
            int row0 = depackRow(p);
            int col0 = depackCol(p);
            int di0 = depackDi(p);
            int pos0 = ind(row0, col0);
            int num0 = numbers[pos0];

            if (rev != 0) {
                if (num0 > nextTarget) {
                    tmpScore -= (num0 * nextTarget);
                }
                state[pos0] = rev-1;
                scstate[pos0] = scstack.pop();
                stepi--;
                continue;
            }

            stack.push(pack(nextTarget, state[pos0]+1, row0, col0, di0));
            scstack.push(scstate[pos0]);

            steps[stepi] = cmdB[di0];
            stepi++;

            if (num0 > nextTarget) {
                tmpScore += (num0 * nextTarget);
            } else if (num0 == nextTarget || (num0 < 0 && state[pos0] == 0)) {
                // calcScore and update solution
                double score = (double)tmpScore * Math.pow((double)nextTarget / (double)N, power);
                if (score > bestScore) {
                    bestStepLen = stepi;
                    System.arraycopy(steps, 0, bestSteps, 0, bestStepLen);
                    bestScore = score;
                }
                nextTarget++;
                if (nextTarget > N) {
                    continue;
                }
            }
            if (stepi == maxSteps) {
                continue;
            }
            state[pos0] = nextTarget;
            scstate[pos0] = tmpScore;

            int dd = row0 == playerRow ? 0 : 1;

            for (int di1 = 0; di1 < 4; di1++) {
                int di = (di1 + dd) & 3;
                int row = move(row0, dt[di]);
                int col = move(col0, dt[di + 1]);
                int pos = ind(row, col);
                if (numbers[pos] == 0) { continue; }
                if (state[pos] < nextTarget) { continue; }
                if (state[pos] > 0 && state[pos] < numbers[pos]) { continue; }
                if (scstate[pos] >= tmpScore) { continue; }

                stack.push(pack(nextTarget, 0, row, col, di));

            }

            for (int di1 = 0; di1 < 4; di1++) {
                int di = (di1 + dd) & 3;
                int row = move(row0, dt[di]);
                int col = move(col0, dt[di + 1]);
                int pos = ind(row, col);
                if (numbers[pos] == 0) { continue; }
                if (state[pos] == nextTarget) { continue; }
                if (state[pos] > 0 && state[pos] < numbers[pos]) { continue; }

                stack.push(pack(nextTarget, 0, row, col, di));

            }

        }
        System.err.printf("len: %d/ %d%n", bestStepLen, maxSteps);
        return new Result(new String(bestSteps, 0, bestStepLen), bestScore);
    }


    private Result solve5() {

        IntStack stack = new IntStack(maxSteps * 4 + 100);
        IntStack scstack = new IntStack(maxSteps);

        for (int di = 0; di < 4; di++) {
            int row = move(playerRow, dt5[(di << 1)]);
            int col = move(playerCol, dt5[(di << 1) + 1]);
            if (numbers[ind(row, col)] != 0) {
                stack.push(pack(1, 0, row, col, di));
            }
        }

        double bestScore = 0;
        byte[] bestSteps = new byte[maxSteps];
        int bestStepLen = 0;

        int[] state = new int[numbers.length];
        int[] scstate = new int[numbers.length];

        int tmpScore = 0;
        byte[] steps = new byte[maxSteps];
        int stepi = 0;

        long time0 = System.currentTimeMillis();
        long time1 = time0;
        long interval = 0L;

        while (!stack.isEmpty()) {
            time0 = time1;
            time1 = System.currentTimeMillis();
            interval = Math.max(interval, time1 - time0);
            if (time1 + interval > timeLimit) {
                break;
            }
            int p = stack.pop();
            int nextTarget = depackNT(p);
            int rev = depackRev(p);
            int row0 = depackRow(p);
            int col0 = depackCol(p);
            int di0 = depackDi(p);
            int pos0 = ind(row0, col0);
            int num0 = numbers[pos0];

            if (rev != 0) {
                if (num0 > nextTarget) {
                    tmpScore -= (num0 * nextTarget);
                }
                state[pos0] = rev-1;
                scstate[pos0] = scstack.pop();
                stepi--;
                continue;
            }

            stack.push(pack(nextTarget, state[pos0]+1, row0, col0, di0));
            scstack.push(scstate[pos0]);

            steps[stepi] = cmdB5[di0];
            stepi++;

            if (num0 > nextTarget) {
                tmpScore += (num0 * nextTarget);
            } else if (num0 == nextTarget || (num0 < 0 && state[pos0] == 0)) {
                // calcScore and update solution
                double score = (double)tmpScore * Math.pow((double)nextTarget / (double)N, power);
                if (score > bestScore) {
                    bestStepLen = stepi;
                    System.arraycopy(steps, 0, bestSteps, 0, bestStepLen);
                    bestScore = score;
                }
                nextTarget++;
                if (nextTarget > N) {
                    continue;
                }
            }
            if (stepi == maxSteps) {
                continue;
            }
            state[pos0] = nextTarget;
            scstate[pos0] = tmpScore;

            int dd = row0 == playerRow ? 0 : 1;

            for (int di1 = 0; di1 < 4; di1++) {
                int di = (di1 + dd) & 3;
                int row = move(row0, dt5[(di << 1)]);
                int col = move(col0, dt5[(di << 1) + 1]);
                int pos = ind(row, col);
                if (numbers[pos] == 0) { continue; }
                if (state[pos] < nextTarget) { continue; }
                if (state[pos] > 0 && state[pos] < numbers[pos]) { continue; }
                if (scstate[pos] >= tmpScore) { continue; }

                stack.push(pack(nextTarget, 0, row, col, di));

            }

            for (int di1 = 0; di1 < 4; di1++) {
                int di = (di1 + dd) & 3;
                int row = move(row0, dt5[(di << 1)]);
                int col = move(col0, dt5[(di << 1) + 1]);
                int pos = ind(row, col);
                if (numbers[pos] == 0) { continue; }
                if (state[pos] == nextTarget) { continue; }
                if (state[pos] > 0 && state[pos] < numbers[pos]) { continue; }

                stack.push(pack(nextTarget, 0, row, col, di));

            }

        }
        System.err.printf("len: %d/ %d%n", bestStepLen, maxSteps);
        return new Result(new String(bestSteps, 0, bestStepLen), bestScore);
    }
}


class IntList {
    int[] buf;
    int length;
    public IntList() {
        this(100);
    }
    public IntList(int cap) {
        buf = new int[cap];
        length = 0;
    }
    public int size() {
        return length;
    }
    public void add(int v) {
        if (buf.length <= length) {
            buf = Arrays.copyOf(buf, length + 100);
        }
        buf[length] = v;
        length++;
    }
    public void set(int i, int v) {
        buf[i] = v;
    }
    public int get(int i) {
        return buf[i];
    }
    public void clear() {
        length = 0;
    }
    public boolean isEmpty() {
        return length == 0;
    }
}

class Tuple<A,B> {
    public final A item1;
    public final B item2;
    public Tuple(A i1, B i2) {
        item1 = i1;
        item2 = i2;
    }
}

final class Result extends Tuple<String, Double> {
    public Result(String cmd, double score) {
        super(cmd, score);
    }
    public String getCommand() {
        return item1;
    }
    public double getScore() {
        return item2.doubleValue();
    }
}

final class IntStack {
    int[] stack;
    int pos = 0;
    public IntStack(int size) {
        stack = new int[size];
    }
    public void push(int value) {
        if (pos >= stack.length) {
            stack = Arrays.copyOf(stack, pos + 10000);
        }
        stack[pos] = value;
        pos++;
    }
    public int pop() {
        pos--;
        return stack[pos];
    }
    public int size() {
        return pos;
    }
    public boolean isEmpty() {
        return pos == 0;
    }
}


class Main {

    static long startTime;

    static int callGetPath(BufferedReader in, VanishingMaze vanishingMaze) throws Exception {

        int _numbersSize = Integer.parseInt(in.readLine());
        int[] numbers = new int[_numbersSize];
        for (int _idx = 0; _idx < _numbersSize; _idx++) {
            numbers[_idx] = Integer.parseInt(in.readLine());
        }
        int playerRow = Integer.parseInt(in.readLine());
        int playerCol = Integer.parseInt(in.readLine());
        double power = Double.parseDouble(in.readLine());

        String _result = vanishingMaze.getPath(numbers, playerRow, playerCol, power);

        long endTime = System.currentTimeMillis();

        System.err.printf("time: %d ms%n", endTime - startTime);
        System.err.flush();

        System.out.println(_result);
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {
        startTime = System.currentTimeMillis();
        System.err.println("FizzBazz!");

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        VanishingMaze vanishingMaze = new VanishingMaze();

        // do edit codes if you need

        callGetPath(in, vanishingMaze);


    }

}
