TopCoder Marathon Match Practice Match VanishingMaze My Solution
================================================================



## Old Platform  

Problem Statement:  
https://community.topcoder.com/longcontest/?module=ViewProblemStatement&rd=17369&pm=15195  



Standings:  
https://community.topcoder.com/longcontest/?module=ViewStandings&rd=17369   





## New Platform  

Problem Statement:  
https://www.topcoder.com/challenges/30091712?tab=details  



Standings:  
https://www.topcoder.com/challenges/30091712?tab=submissions    



Review Page:  
https://software.topcoder.com/review/actions/ViewProjectDetails?pid=30091712  

